package com.gmail.mcraftworldmc.board;

import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	Board board = new Board(this);
	public void onEnable(){
		saveDefaultConfig();
		Bukkit.getPluginManager().registerEvents(this, this);
		if(Bukkit.getOnlinePlayers().length > 0){
			int staffCount = 0;
			for(Player p : Bukkit.getOnlinePlayers()){
				if(p.hasPermission("purge.staff")) staffCount++;
			}
			for(Player p : Bukkit.getOnlinePlayers()){
				board.setBoard(p, ChatColor.translateAlternateColorCodes('&', "&lPurgeMC"), Bukkit.getOnlinePlayers().length, staffCount);
			}
		}
		if(getConfig().getBoolean("bar.enable")){
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
				int time = 60;
				int place = 4;
				public void run(){
					time--;
					switch(time){
					case 40:
						place = 4; break;
					case 30:
						place = 3; break;
					case 20:
						place = 2; break;
					case 10:
						place = 1; break;
					}
					for(Player p : Bukkit.getOnlinePlayers()){
						switch(place){
						case 4:
							BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&', getConfig().getString("bar.message" + place)), 75.0F); break;
						case 3:
							BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&', getConfig().getString("bar.message" + place)), 50.0F); break;
						case 2:
							BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&', getConfig().getString("bar.message" + place)), 25.0F); break;
						case 1:
							BarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('&', getConfig().getString("bar.message" + place)), 1.0F); break;
						}
					}
					if(time == 0) time = 50;
				}
			}, 0l, 20l);
		}
	}
	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent e){
		
		int staffCount = 0;
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.hasPermission("purge.staff")) staffCount++;
		}
		final int SF = staffCount;
		Bukkit.getScheduler().runTaskLater(this, new Runnable(){
			public void run(){
				for(Player p : Bukkit.getOnlinePlayers()){
					board.setBoard(p, ChatColor.translateAlternateColorCodes('&', "&lThe Purge"), Bukkit.getOnlinePlayers().length, SF);
				}
			}
		}, 2L);
	}
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		if(Bukkit.getOnlinePlayers().length == 0){
			return;
		}
		Bukkit.getScheduler().runTaskLater(this, new Runnable(){
			public void run(){
				int staffCount = 0;
				for(Player p : Bukkit.getOnlinePlayers()){
					if(p.hasPermission("purge.staff")) staffCount++;
				}
				for(Player p : Bukkit.getOnlinePlayers()){
					board.setBoard(p, ChatColor.translateAlternateColorCodes('&', "&lThe Purge"), Bukkit.getOnlinePlayers().length, staffCount);
				}
			}
		}, 2L);
	}
}
