package com.gmail.mcraftworldmc.board;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class Board {
	@SuppressWarnings("unused")
	private Main plugin;
	public Board(Main instance){
		this.plugin = instance;
	}
	public void setBoard(Player p, String title, int playCount, int staffCount){
		ScoreboardManager sm = Bukkit.getScoreboardManager();
		Scoreboard board = sm.getNewScoreboard();
		Objective purge = board.registerNewObjective("purge", "dummy");
		purge.setDisplayName(title);
		purge.setDisplaySlot(DisplaySlot.SIDEBAR);
		Score online = purge.getScore(ChatColor.translateAlternateColorCodes('&', "&a&lPlayers"));
		online.setScore(10);
		Score onlineCount = purge.getScore(ChatColor.BOLD + "" + playCount);
		onlineCount.setScore(9);
		Score space = purge.getScore(" ");
		space.setScore(8);
		Score staffOnline = purge.getScore(ChatColor.translateAlternateColorCodes('&', "&c&lOnline Staff"));
		staffOnline.setScore(7);
		Score websiteTitle = purge.getScore(ChatColor.translateAlternateColorCodes('&', "&b&lWebsite"));
		websiteTitle.setScore(4);
		Score website = purge.getScore("purge-mc.com");
		website.setScore(3);
		Score finish = purge.getScore("-----------");
		finish.setScore(2);
		if(staffCount == 0){
			Score staffOnlineString = purge.getScore(ChatColor.BOLD + "None");
			staffOnlineString.setScore(6);
			Score space2 = purge.getScore("  ");
			space2.setScore(5);
			p.setScoreboard(board);
			return;
		}
		Score staffOnlineString = purge.getScore(Integer.toString(staffCount));
		staffOnlineString.setScore(6);
		Score space2 = purge.getScore(("  "));
		space2.setScore(5);
		p.setScoreboard(board);
	}
}
